import React, { Component } from 'react';
import './App.css';

export default class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isaChecked: false,
      isbChecked: false,
      iscChecked: false,
      usersArr: [],
      firstname: "",
      lastname: "",
      activity: ""
    }
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleFirstName = (event) => {
    this.setState({ firstname: event.target.value });
  }

  handleLastName = (event) => {
    this.setState({ lastname: event.target.value });
  }

  handleActivity = (event) => {
    this.setState({ activity: event.target.value });
  }

  handleInputChange(event) {
    let isaCheckedCopy = this.state.isaChecked,
      isbCheckedCopy = this.state.isbChecked,
      iscCheckedCopy = this.state.iscChecked;

    if (event.target.name === "a") {
      isaCheckedCopy = !isaCheckedCopy;
    }
    if (event.target.name === "b") {
      isbCheckedCopy = !isbCheckedCopy;
    }
    if (event.target.name === "c") {
      iscCheckedCopy = !iscCheckedCopy;
    }

    this.setState({
      isaChecked: isaCheckedCopy,
      isbChecked: isbCheckedCopy,
      iscChecked: iscCheckedCopy
    });
  }

  handleClick = () => {
    let restrictionsCopy = "";

    if (this.state.isaChecked && this.state.isbChecked && this.state.iscChecked) {
      restrictionsCopy = "abc";
    }
    else if (this.state.isaChecked && this.state.isbChecked) {
      restrictionsCopy = "ab";
    }
    else if (this.state.isbChecked && this.state.iscChecked) {
      restrictionsCopy = "bc";
    }
    else if (this.state.isaChecked && this.state.iscChecked) {
      restrictionsCopy = "ac";
    }
    else if (this.state.isaChecked) {
      restrictionsCopy = "a";
    }
    else if (this.state.isbChecked) {
      restrictionsCopy = "b";
    }
    else if (this.state.iscChecked) {
      restrictionsCopy = "c";
    }
    else {
      restrictionsCopy = "";
    }

    this.state.usersArr.push({
      "firstname": this.state.firstname,
      "lastname": this.state.lastname,
      "activity": this.state.activity,
      "restrictions": restrictionsCopy
    });

    this.setState({
      isaChecked: false,
      isbChecked: false,
      iscChecked: false,
      firstname: "",
      lastname: "",
      activity: "Science Lab"
    });

    console.log(this.state.usersArr);
  }

  removeUser=(index)=>{
    let usersArrCopy = this.state.usersArr.slice();

    usersArrCopy.splice(index, 1);

    this.setState({
      usersArr: usersArrCopy
    });
  }

  render() {
    return (
      <div className="App">
        <div className="DisplayBlockClass">
          <label>
            First Name:
          <input type="text"
              name="firstname"
              className="InputClass"
              value={this.state.firstname}
              onChange={this.handleFirstName} />
          </label>
        </div>
        <div className="DisplayBlockClass">
          <label>
            Last Name:
          <input type="text"
              name="lastname"
              className="InputClass"
              value={this.state.lastname}
              onChange={this.handleLastName} />
          </label>
        </div>
        <div className="DisplayBlockClass">
          <label>
            Select Activity:
          <select className="InputClass"
              style={{ width: 183 }}
              onChange={this.handleActivity}
              value={this.state.activity}
            >
              <option value="Science Lab">Science Lab</option>
              <option value="Swimming">Swimming</option>
              <option value="Cooking">Cooking</option>
              <option value="Painting">Painting</option>
            </select>
          </label>
        </div>
        <div className="DisplayBlockClass">
          <label>
            Check all that apply:
            <label className="CheckboxCSS">
              <input type="checkbox"
                name="a"
                checked={this.state.isaChecked}
                onChange={this.handleInputChange} />
              a) Dietary Restrictions
            </label>
            <label className="CheckboxCSS">
              <input type="checkbox"
                name="b"
                checked={this.state.isbChecked}
                onChange={this.handleInputChange} />
              b) PhysicalDisablities
            </label>
            <label className="CheckboxCSS">
              <input type="checkbox"
                name="c"
                checked={this.state.iscChecked}
                onChange={this.handleInputChange} />
              c) Medical Needs
            </label>
          </label>
        </div>
        <button type="submit" name="submit" onClick={this.handleClick}>Submit</button>
        <UserComponentLoop usersArray={this.state.usersArr} removeUser={()=>this.removeUser}/>
      </div>
    );
  }
}

function Text(props) {
  var style = {
    border: "1px solid black",
    width: props.width
  }
  return (
    <div style={style}>{props.text}</div>
  )
}

function Button(props) {
  return (
    <div>
      <button onClick={props.removeUser}>X</button>
    </div>
  )
}

function UserComponent(props) {
  return (
    <tr>
      <td>
        <Button removeUser={props.removeUser} />
      </td>
      <td>
        <Text text={props.firstname} width="300" />
      </td>
      <td>
        <Text text={props.lastname} width="300" />
      </td>
      <td>
        <Text text={props.activity} width="300" />
      </td>
      <td>
        <Text text={props.restrictions} width="300" />
      </td>
      </tr>
  )
}

function UserComponentLoop(props) {
  return (
    <div>
      <table className="table table-bordered">
        <tr>
          <th>Remove</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Activity</th>
          <th>Restrictions</th>
        </tr>
        {
          props.usersArray.map((item, index) => {
            return <UserComponent key={index}
                         firstname={item.firstname}
                         lastname={item.lastname}
                         activity={item.activity}
                         restrictions={item.restrictions}
                         removeUser={props.removeUser(index)}
            />
          })
        }
      </table>
    </div>
  )
}